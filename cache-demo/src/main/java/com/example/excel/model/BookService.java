package com.example.excel.model;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional
    public Book getBookByIsbn(String isbn) {
        Book cachedBook = bookRepository.findById(isbn).orElse(null);

        if (cachedBook != null) {
            return new Book(cachedBook.getIsbn(), cachedBook.getTitle());
        } else {
            SlowService();
            Book newBook = new Book(isbn, "Book here !! ");
            Book newCachedBook = new Book(isbn, newBook.getTitle());
            bookRepository.save(newCachedBook);
            return newBook;
        }
    }

    private void SlowService() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
